import { Router } from "express";
import { hex_to_rgb, rgb_to_hex } from "./converter.js"

const routes = Router();

// Endpoint GET '{{api_url}}/'
routes.get("/", (req, res) => res.status(200).send("Welcome!"));

// Endpoint GET /rgb-to-hex?red=255&green=136&blue=0
routes.get("/rgb-to-hex", (req, res) => {
    const RED = parseFloat(req.query.red);
    const GREEN = parseFloat(req.query.green);
    const BLUE = parseFloat(req.query.blue);
    const HEX = rgb_to_hex(RED, GREEN, BLUE); // Integraatio
    res.status(200).send(HEX);
})

// Endpoint GET /hex-to-rgb?hex=ff8800
routes.get("/hex-to-rgb", (req, res) => {
    const hex_str = "#"+req.query.hex;
    const RGB = hex_to_rgb(hex_str);
    const response = `${RGB[0]}-${RGB[1]}-${RGB[2]}`
    res.status(200).send(response);
})

export default routes;