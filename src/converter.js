/**
 * Padding hex component if necessary.
 * Hex component representation requires
 * two hexadecimal characters.
 * @param {string} comp 
 * @returns {string} two hexadecimal characters.
 */
const pad = (comp) => {
    let padded = comp.length == 2 ? comp : "0" + comp;
    // let padded = comp;
    // if (comp.length < 2) {
    //     padded = "0" + comp;
    // }
    return padded;
};

/**
 * RGB-to-HEX conversion        
 * @param {number} r RED 0-255
 * @param {number} g GREEN 0-255
 * @param {number} b BLUE 0-255
 * @returns {string} in hex color format, e.g. "#00ff00" (green)
 */
export const rgb_to_hex = (r, g, b) => {
    const HEX_RED = r.toString(16);
    const HEX_GREEN = g.toString(16);
    const HEX_BLUE = b.toString(16);
    return "#" + pad(HEX_RED) + pad(HEX_GREEN) + pad(HEX_BLUE);
};


/**
 *  HEX-to-RGB conversion
 * @param {string} hex_str color string in a hex format
 * @returns {array[number, number, number]} color in rgb format.
 */
export const hex_to_rgb = (hex_str) => {
    let RGB_RED = parseInt(hex_str.slice(1,3), 16);
    let RGB_GREEN = parseInt(hex_str.slice(3,5), 16);
    let RGB_BLUE = parseInt(hex_str.slice(5,7), 16);
    //const response = `${RGB[0]}-${RGB[1]}-${RGB[2]}`
    return [RGB_RED, RGB_GREEN, RGB_BLUE];
};